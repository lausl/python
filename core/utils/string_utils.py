#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's String Utils
# -- 字符串工具集
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年3月8日 15:46:13
# ----------------------------------------------------------
import re


def class_camelcase_generate(class_name_str):
    """
        生成驼峰式类名

        :param str class_name_str:  类名
        :return:                    驼峰式类名
        :rtype: str
    """
    class_name_new = ""
    if re.search("[_]+", class_name_str):
        values = class_name_str.split("_")
        for val in values:
            if val:
                class_name_new += val[0].upper() + val[1:]
    else:
        return class_name_str[0].upper() + class_name_str[1:]
    return class_name_new


def column_camelcase_generate(column_name_str):
    """
        生成驼峰式字段名

        :param str column_name_str: 字段名
        :return:                    驼峰式字段名
        :rtype: str
    """
    if re.search("[_]+", column_name_str):
        values = column_name_str.split("_")
        row = 0
        for val in values:
            if row == 0:
                column_name_str = val
            else:
                if val:
                    column_name_str += val[0].upper() + val[1:]
            row += 1
    return column_name_str
