#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's File Utils
# -- 文件处理工具集
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年3月8日 15:53:23
# ----------------------------------------------------------
import os


def write_file(file_name, file_suffix, content, output_path):
    """
        内容写入文件

        :param str file_name:   文件名
        :param str file_suffix: 文件后缀
        :param str content:     内容
        :param str output_path: 输出路径
        :return:                None
    """
    if not file_name:
        print("File name is None.")
        return None
    if not file_suffix:
        print("File suffix is None.")
        return None
    if not output_path:
        print("Output path is None.")
        return None
    # 目录是否存在
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    f = open(output_path + os.path.sep + file_name + file_suffix, 'w+', encoding='UTF-8')
    print(content)
    f.write(content)
    f.close()
    print("*********************** 文件写入成功 ***********************")
