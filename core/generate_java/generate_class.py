#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Generate Class
# -- 生成 Java 文件
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年3月9日 10:06:36
# ----------------------------------------------------------
import os
from core.generate_java import dispose_java_class
from core.utils import read_config, file_utils, string_utils


def generate_java_model(rs_path):
    """
        生成Java实体

        :param rs_path:
        :return:
    """
    yml = read_config.read_yml(rs_path + "class_config.yml")
    class_cn = yml["class_cn"]
    dispose_mode = class_cn["dispose_mode"]
    package_name = class_cn["package_name"]
    class_name = string_utils.class_camelcase_generate(class_cn["class_name"])
    class_content = dispose_java_class.get_top(class_name, class_cn) \
                    + dispose_java_class.get_body(class_name, class_cn, rs_path + "input.txt")
    # 生成文件
    sep = os.path.sep
    output_path = rs_path + sep + "out" + sep + dispose_mode + sep + package_name
    file_utils.write_file(class_name, ".java", class_content, output_path)


def generate_enum_constant(rs_path):
    """
        生成枚举常量

        :param rs_path:
        :return:
    """
    enum_constant = dispose_java_class.generate_enum_constant(rs_path + "input.txt")
    # 生成文件
    sep = os.path.sep
    output_path = rs_path + sep + "out" + sep
    file_utils.write_file("enum", ".txt", enum_constant, output_path)


if __name__ == "__main__":
    resources_path = os.path.abspath("../../") + "/resources/"
    # 生成Java实体
    generate_java_model(resources_path)
    # 生成枚举常量
    # generate_enum_constant(resources_path)
