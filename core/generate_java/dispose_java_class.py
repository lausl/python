#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Dispose Java Class
# -- 处理 Java 类（拼装实体）
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年3月8日 16:37:42
# ----------------------------------------------------------
import re
from core.utils import string_utils


def get_top(class_name, class_cn):
    """
        获取实体类顶部

        :param str class_name:  类名
        :param object class_cn: 类配置
        :return:                实体类顶部内容
    """
    # 处理方式
    dispose_mode = class_cn["dispose_mode"]
    # 包名
    package_name = class_cn["package_name"]
    # 包路径
    package = class_cn["package_url"] % (dispose_mode, package_name)
    # 继承或实现类
    implement_class = class_cn["implement_class"]
    # 实体类描述
    class_desc = class_cn["class_desc"]
    # 引入类
    it_class = ""
    for item in class_cn["import_class"]:
        it_class += item
    import_class = it_class % implement_class
    import_class_add = ""
    if "request" == dispose_mode:
        for item in class_cn["import_not_public_class"]:
            import_class_add += item
    import_class_add += "\n"
    import_class += import_class_add
    annotation = "/**\n" \
                 " * " + class_name + "\n" \
                 " * -- " + class_desc + "\n" \
                 " *\n" \
                 " * @author " + class_cn["author"] + "\n" \
                 " * @date " + class_cn["date"] + "\n" \
                 " * @qq " + str(class_cn["qq"]) + "\n" \
                 " * @email " + class_cn["email"] + "\n" \
                 " */\n"
    # 实体类注解
    it_class_annotation = ""
    for item in class_cn["class_annotation"]:
        it_class_annotation += item
    class_annotation = it_class_annotation % class_desc
    return package + import_class + annotation + class_annotation


def get_body(class_name, class_cn, input_file):
    """
        获取实体类主体

        :param str class_name:  类名
        :param object class_cn: 类配置
        :param file input_file: 读取文件
        :return:                实体类主题
    """
    body = "public class " + class_name + " " + class_cn["implement_mode"] + " " + class_cn["implement_class"] + " {\n"
    f = open(input_file, 'r', encoding='UTF-8')
    cn = len(f.readlines())
    f.close()
    rf = open(input_file, 'r', encoding='UTF-8')
    r = 0
    for line in rf:
        r += 1
        # 字段正则
        p1 = re.compile(r"[a-z_]+", re.I)
        column = p1.match(line).group(0)
        # 字段驼峰式命名
        column_is_use_camelcase = class_cn["column_is_use_camelcase"]
        if column_is_use_camelcase:
            column = string_utils.column_camelcase_generate(column)
        # 字段描述正则
        p2 = re.compile("[^a-zA-Z_\s][\u4e00-\u9fa5\d\s\S]+[a-zA-Z_\s\S]*[^\n]")
        desc = p2.search(line).group(0)
        column_desc = "\t/**\n\t * " + desc + "\n\t */\n"
        # TODO 接口字段类型处理暂未处理
        column_definition = "\tprivate String " + column
        # 字段注解
        column_annotation = ""
        for annotation in class_cn["column_annotation"]:
            column_annotation += annotation
        column_annotation = column_annotation % desc
        # 校验必填正则
        p3 = re.compile("[\u5fc5][\u586b]")
        if p3.search(line) and "request" == class_cn["dispose_mode"]:
            # [简短]字段描述正则
            ds = re.search("[\u4e00-\u9fa5]+", desc).group(0)
            # 字段非公用的注解
            column_not_public_annotation = ""
            for not_public_annotation in class_cn["column_not_public_annotation"]:
                column_not_public_annotation += not_public_annotation
            body += column_desc + column_not_public_annotation % ds + column_annotation + column_definition + ";\n"
        else:
            body += column_desc + column_annotation + column_definition + ";\n"
        if cn != r:
            body += "\n"
        else:
            body += "}"
    rf.close()
    return body


def generate_enum_constant(input_file):
    """
        生成枚举常量

        :param file input_file: 读取文件
        :return:                枚举常量
    """
    f = open(input_file, 'r', encoding='UTF-8')
    content = ""
    for line in f:
        p1 = re.compile(r"[a-z_\d]+", re.I)
        column = p1.match(line).group(0)
        column_upper = column.upper()
        p2 = re.compile("[^a-zA-Z_\s][\u4e00-\u9fa5\d\s\S]+[a-zA-Z_\s\S]*[^\n]")
        desc = p2.search(line).group(0)
        content += column_upper + "(\"" + column + "\", \"" + desc + "\"),\n"
    return content
    f.close()
